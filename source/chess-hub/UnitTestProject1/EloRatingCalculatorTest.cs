﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Chesshub.Models;

namespace ChessHub.Util.Tests
{
    [TestClass]
    public class EloRatingCalculatorTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            Player whitePlayer = new Player();
            Player blackPlayer = new Player();
            whitePlayer.EloRating = 1500;
            blackPlayer.EloRating = 1500;

            int[] actual = EloRatingCalculator.RatingChanges(whitePlayer, blackPlayer, GameResultsEnum.WhiteVictory);

            Assert.AreEqual(1516, actual[0]);
            Assert.AreEqual(1484, actual[1]);
        }
    }
}
