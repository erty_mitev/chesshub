﻿namespace StartUp
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    

    class StartUp
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Ratings of A and B players");
            int currRatA = int.Parse(Console.ReadLine());
            int currRatB = int.Parse(Console.ReadLine());
            Console.WriteLine("The result of the game (Win = W, loss = L, tie = T)");//  and getting the actual pts from the dictionary
            double actualEloA = EloRating.EloRating.actualRating[char.Parse(Console.ReadLine())];
            double actualEloB = EloRating.EloRating.actualRating[char.Parse(Console.ReadLine())];
            //Expected Ratings of A and B players from the game
            double expEloA = EloRating.EloRating.ExpectedRatingPts(currRatA, currRatB);
            //Console.WriteLine(expEloA);
            double expEloB = EloRating.EloRating.ExpectedRatingPts(currRatB, currRatA);
            //Console.WriteLine(expEloB);
            //Getting the final Elo Rating 
            int finalRatingA = EloRating.EloRating.FinalizedRating(currRatA, expEloA, actualEloA);
            Console.WriteLine("Final rating of player A " + finalRatingA);
            int finalRatingB = EloRating.EloRating.FinalizedRating(currRatB, expEloB, actualEloB);
            Console.WriteLine("Final rating of player B " + finalRatingB);
        }
    }
}
