﻿namespace ChessHub.Util
{
    using System;
    using System.Collections.Generic;
    using Chesshub.Models;
    public class EloRatingCalculator
    {
        private static byte ReturnKFactor(int rating)
        {
            if (rating < 2100)
            {
                return 32;
            }
            else if (rating < 2400)
            {
                return 24;
            }
            else
            {
                return 16;
            }

        }

        private static double[] ReturnActualElo(GameResultsEnum gameResult)
        {
            double[] result = new double[2];
            switch (gameResult)
            {
                case GameResultsEnum.WhiteVictory :
                    result[0] = actualRating[GameResultsEnum.WhiteVictory];
                    result[1] = actualRating[GameResultsEnum.BlackVictory];
                    break;
                case GameResultsEnum.BlackVictory:
                    result[0] = actualRating[GameResultsEnum.BlackVictory];
                    result[1] = actualRating[GameResultsEnum.WhiteVictory];
                    break;
                default:
                    result[0] = actualRating[GameResultsEnum.GameDrawn];
                    result[1] = actualRating[GameResultsEnum.GameDrawn];
                    break;
            }
            return result;
        }
        private static double[] ExpectedRatingPts(int WhitePLayerRating, int BlackPlayerRating)
        {
            // Calculating Elo with true strength(current rating) of the players
            double difference = WhitePLayerRating - BlackPlayerRating;
            double[] eloRating = new double[2];
            eloRating[0]= 1 / (1 + Math.Pow(10, (difference / 400)));
            eloRating[1] = 1 / (1 + Math.Pow(10, ((-1 * difference) / 400)));

            return eloRating;
        }
        //Dictionary for win/losses/ties points
        private static Dictionary<GameResultsEnum, double> actualRating = new Dictionary<GameResultsEnum, double>
        {
            {GameResultsEnum.WhiteVictory, 1 },
            {GameResultsEnum.BlackVictory, 0 },
            {GameResultsEnum.GameDrawn, 0.5 }
        };
        //Calculation of the Final Elo rating
        public static int[] RatingChanges(Player whitePlayer, Player blackPlayer, GameResultsEnum gameResult)
        {
            byte whiteKFactor = ReturnKFactor(whitePlayer.EloRating);
            byte blackKFactor = ReturnKFactor(blackPlayer.EloRating);

            int[] finalRatings = new int[2];
            finalRatings[0] = whitePlayer.EloRating + (int)(whiteKFactor * (ReturnActualElo(gameResult)[0] - ExpectedRatingPts(whitePlayer.EloRating, blackPlayer.EloRating)[0]));
            finalRatings[1] = blackPlayer.EloRating + (int)(blackKFactor * (ReturnActualElo(gameResult)[1] - ExpectedRatingPts(whitePlayer.EloRating, blackPlayer.EloRating)[1]));
            return finalRatings;
        }
    }
}
