﻿namespace EloRating
{

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

    public class EloRating
    {
        public static double ExpectedRatingPts(int ratA, int ratB)
        {
            // Calculating Elo with true strength(current rating) of the players
            double difference = ratA - ratB;
            double eloRating = 1 / (1 + Math.Pow(10, (difference / 400)));

            return eloRating;
        }
        //Dictionary for win/losses/ties points
        public static Dictionary<char, double> actualRating = new Dictionary<char, double>
        {
            {'W', 1 },
            {'L', 0 },
            {'T', 0.5 }
        };
        //Calculation of the Final Elo rating
        public static int FinalizedRating(int ratA, double expElo, double actualElo)
        {
            int k;
            if (ratA < 2100)
            {
                k = 32;
            }
            else if (ratA >= 2100 && ratA < 2400)
            {
                k = 24;
            }
            else
            {
                k = 16;
            }
            int finalRating = ratA + (int)(k * (actualElo - expElo));
            return finalRating;
        }
    }
}
