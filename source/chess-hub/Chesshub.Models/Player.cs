﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chesshub.Models
{
    public class Player
    {
        private ICollection<Game> games;

        public Player()
        {
            this.games = new HashSet<Game>();
        }
        public int Id { get; set; }

        public int EloRating { get; set; }

        public string DisplayName { get; set; }

        public virtual ICollection<Game> Games
        {
            get { return this.games; }
            set { this.games = value; }
        }

    }
}
