﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chesshub.Models
{
    public enum GameResultsEnum
    {
        WhiteVictory = 0,
        BlackVictory = 1,
        GameDrawn = 2,
    }
}
