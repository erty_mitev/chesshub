﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chesshub.Models
{
    public class Game
    {
        private ICollection<Move> moves;

        public Game()
        {
            this.moves = new HashSet<Move>();
        }

        public int Id { get; set; }

        public GameResultsEnum Result { get; set; }

        public Player White { get; set; }

        public Player Black { get; set; }

        /// <summary>
        /// total moves time for EACH player in seconds
        /// </summary>
        public int TimeControl { get; set; }

        public virtual ICollection<Move> Moves
        {
            get { return this.moves; }
            set { this.moves = value; }
        }

    }
}
