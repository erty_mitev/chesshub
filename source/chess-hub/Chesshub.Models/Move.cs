﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chesshub.Models
{
    public class Move
    {
        public Move()
        {
            this.Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }

        public int MoveIndex { get; set; }

        public bool IsCastling { get; set; }

        public string PieceNotation { get; set; }

        public byte StartPosition { get; set; }

        public byte EndPosition { get; set; }

        public int GameId { get; set; }

        public virtual Game Game { get; set; }

    }
}
