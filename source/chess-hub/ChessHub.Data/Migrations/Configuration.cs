namespace ChessHub.Data.Migrations
{
    using Chesshub.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ChessHub.Data.ChessHubDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(ChessHub.Data.ChessHubDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            context.Players.AddOrUpdate(
                p => p.DisplayName,
                new Player { DisplayName = "Pesho", EloRating = 1500 },
                new Player { DisplayName = "Gosho", EloRating = 1500 }
                );

            base.Seed(context);
        }
    }
}
