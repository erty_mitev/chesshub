﻿using Chesshub.Models;
using System.Data.Entity;

namespace ChessHub.Data
{
    public class ChessHubDbContext : DbContext
    {
        public ChessHubDbContext()
            : base("ChessHub")
        {
        }
        public virtual IDbSet<Player> Players { get; set; }

        public virtual IDbSet<Game> Games { get; set; }

        public virtual IDbSet<Move> Moves { get; set; }
    }
}
