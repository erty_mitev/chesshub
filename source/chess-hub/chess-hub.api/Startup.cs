﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;
using ChessHub.Data;
using System.Data.Entity.Migrations;
using Chesshub.Models;

[assembly: OwinStartup(typeof(ChessHub.api.Startup))]

namespace ChessHub.api
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
