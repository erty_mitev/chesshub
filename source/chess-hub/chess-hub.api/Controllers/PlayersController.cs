﻿using Chesshub.Models;
using ChessHub.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;

namespace ChessHub.api.Controllers
{
    public class PlayersController : ApiController
    {
        private ChessHubDbContext context = new ChessHubDbContext();
        public PlayersController()
        {
        }
        public IEnumerable<Player> Get()
        {
            var players = context.Players.ToList();
            return players;
        }
        public Player Get(int id)
        {
            Player player = new Player();
            
            player = context.Players.FirstOrDefault(p => p.Id == id);

            return player;
        }
    }
}
