﻿chessHub.controller("PlayerListController", ["$scope", "ChessHubApi", function ($scope, ChessHubApi) {
    $scope.PlayerListViewModel = {};
    $scope.PlayerListViewModel.players = [];

    function getData() { 
        ChessHubApi.getAllPlayers().then(function successCallback(response) {
            $scope.PlayerListViewModel.players = response.data;
        }, function errorCallback(response) {
            console.log(response.status);
        });
    };

    getData();

    $scope.inversionContainer = {};
    $scope.inversionContainer.getData = getData;
}]);