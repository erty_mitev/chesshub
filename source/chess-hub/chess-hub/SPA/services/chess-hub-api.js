﻿chessHub.service("ChessHubApi", ["$http", function ($http) {

    function getAllPlayers() {
        return $http({
            method: "GET",
            url: "http://localhost:64948/api/players"
        });
    }

    return {
        getAllPlayers: getAllPlayers
    }
}]);