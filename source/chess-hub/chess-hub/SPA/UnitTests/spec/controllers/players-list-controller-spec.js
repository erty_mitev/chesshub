﻿describe('PlayerListController', function () {
    beforeEach(module('ChessHub'));

    var $controller, ChessHubApi, $q, $rootScope, $httpBackend;

    beforeEach(inject(function (_$controller_, _ChessHubApi_, _$q_, _$rootScope_, _$httpBackend_) {
        // The injector unwraps the underscores (_) from around the parameter names when matching
        $controller = _$controller_;
        ChessHubApi = _ChessHubApi_;
        $q = _$q_;
        $rootScope = _$rootScope_;
        $httpBackend = _$httpBackend_;
        $httpBackend.when("GET", /^/).respond(200, '');
    }));

    describe("$scope.getData", function () {
        it("should call the api service, and store the data in the viewModel object", function () {
            
            var deffered = new $q.defer();
            deffered.resolve({
                status: 200,
                data:"test"
            })
            var $scope = $rootScope.$new();
            var controller = $controller('PlayerListController', { $scope: $scope });
            spyOn(ChessHubApi, "getAllPlayers").and.returnValue(deffered.promise);
            $scope.inversionContainer.getData();
            expect(ChessHubApi.getAllPlayers).toHaveBeenCalled();
            $rootScope.$digest();
            expect($scope.PlayerListViewModel.players).toEqual("test");
        });

        it("should call the api service, and log and error if the call fails", function () {

            var deffered = new $q.defer();
            deffered.reject({
                status: 200,
                data: "test"
            });
            var $scope = $rootScope.$new();
            var controller = $controller('PlayerListController', { $scope: $scope });
            spyOn(ChessHubApi, "getAllPlayers").and.returnValue(deffered.promise);
            $scope.inversionContainer.getData();
            expect(ChessHubApi.getAllPlayers).toHaveBeenCalled();
            $rootScope.$digest();
            expect($scope.PlayerListViewModel.players).toEqual([]);
        });
    });
});