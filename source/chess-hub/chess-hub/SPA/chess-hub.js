﻿var chessHub = angular.module("ChessHub", ["ngRoute"])
    .config(['$locationProvider', '$routeProvider',
    function config($locationProvider, $routeProvider) {
        $locationProvider.hashPrefix('!');

        $routeProvider.
              when('/start', {
                  template: '<home></home>'
              })
             .when('/end', {
                  template: '<end></end>'
             })
            .when('/chess', {
                template: '<chess></chess>'
            })
            .otherwise('/start');
    }
])