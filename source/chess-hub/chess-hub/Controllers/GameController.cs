﻿using System.Net.Mime;
using System.Web.Mvc;

namespace ChessHub.Website.Controllers
{
    public class GameController : Controller
    {
        // GET: Game
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Tests() {
            return File("~/SPA/UnitTests/tests.html", MediaTypeNames.Text.Html);
        }
    }
}