﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ChessHub.Website.Startup))]
namespace ChessHub.Website
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
