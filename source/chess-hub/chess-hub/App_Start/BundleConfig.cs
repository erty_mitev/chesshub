﻿using System.Web;
using System.Web.Optimization;

namespace ChessHub
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/libs/dev/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/libs/dev/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                        "~/Scripts/libs/dev/angular.js",
                        "~/Scripts/libs/dev/angular-route.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/libs/dev/bootstrap.js",
                      "~/Scripts/libs/dev/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/spa").Include(
                      "~/SPA/chess-hub.js",
                      "~/SPA/components/player-list.js",
                      "~/SPA/controllers/player-list-controller.js",
                      "~/SPA/services/chess-hub-api.js",
                      "~/SPA/controllers/HomeController.js",
                      "~/SPA/components/home.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));
        }
    }
}
